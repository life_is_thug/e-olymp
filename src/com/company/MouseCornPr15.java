package com.company;

import java.util.Scanner;

public class MouseCornPr15 {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int[][] a = new int[m][n];
        String[][] path = new String[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {

                a[i][j] = sc.nextInt();
            }
        }

        int[][] max = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = n - 1 ; j > -1; j--) {

                if (i == 0 && j == n - 1) {
                    max[i][j] = a[i][j];
                    path[i][j] = "";
                }
                else
                if (j == n - 1 && i != 0) {

                    max[i][j] = a[i][j] + max[i - 1][j];
                    path[i][j] = "F" + path[i - 1][j];
                }
                else if (i == 0) {

                    max[i][j] = a[i][j] + max[i][j + 1];
                    path[i][j] = "R" + path[i][j + 1];
                }
                else {

                  //  max[i][j] = Math.max(max[i][j + 1], max[i - 1][j]) + a[i][j];
                    if (Math.max(max[i][j + 1], max[i - 1][j]) == max[i][j + 1]){

                        max[i][j] = a[i][j] + max[i][j + 1];
                        path[i][j] = "R" + path[i][j + 1];

                    } else {

                        max[i][j] = a[i][j] + max[i - 1][j];
                        path[i][j] = "F" + path[i - 1][j];
                    }

                }
            }

        }
        System.out.println(path[m - 1] [0]);
    }
}
