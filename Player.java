package com.company;

import java.io.InputStream;
import java.util.Scanner;


public class Player {


    Side side ;
    String name;
    InputStream is;

    public Player(Side side, InputStream is){
        this.is = is;
        System.out.print("Player name:");
        this.name = new Scanner(is).nextLine();
        System.out.println();
        this.side = side;
    }


}
