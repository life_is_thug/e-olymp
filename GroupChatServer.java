package com.company;

import jdk.internal.util.xml.impl.Input;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ulker on 27.04.2016.
 */
public class GroupChatServer {

    private ServerSocket serverSocket;
    int counter = 0;
    InputStream responseWatcher;
    private HashMap<Integer, Socket> socketHashMap;
    GroupChatServer(String ip, int port, int maxConns) throws UnknownHostException, IOException{

        socketHashMap = new HashMap<>(50);
        serverSocket = new ServerSocket(port, maxConns, InetAddress.getByName(ip));

    }


    private void feedback(int clientId, String message){

        try {
            for (Map.Entry<Integer, Socket> socketEntry : socketHashMap.entrySet()){


                if (socketEntry.getKey() == clientId)
                    continue;

                socketEntry.getValue().getOutputStream().write(("Client " + clientId + " :" +  message + "\n" ).getBytes());
                socketEntry.getValue().getOutputStream().flush();
            }
        } catch (IOException ioe){

            System.out.println("ioe");
        }

    }

    public void listen()throws  IOException{




        while (true) {
            System.out.println("Waiting for clients...");
            final Socket socket = serverSocket.accept();
            System.out.println("New client accepted");
            socketHashMap.put(counter++, socket);

            new Thread(){

                @Override
                public void run() {


                    InputStream is = null;
                    Socket threadSocket = socket;
                    try {
                        is = threadSocket.getInputStream();
                    } catch (IOException ioe){

                        System.out.println("IOException when getting is from client");

                    }
                    int clientId = counter - 1;
                    //InputStreamReader isr = new InputStreamReader(is);
                    String line = "";
                    int a = -2;
                    try{
                        while ( true) {
                            if (!threadSocket.isInputShutdown()) {
                                while ((a = is.read()) != '\n') {

                                    line += (char) a;

                                }
                            } else {
                                System.out.println("Client " + clientId + " input stream is shutdown");
                                threadSocket.shutdownInput();
                                threadSocket.shutdownOutput();
                                socket.close();
                                socketHashMap.remove(clientId);
                            }

                            if (line.equals("exit")){
                                threadSocket.shutdownInput();
                                threadSocket.shutdownOutput();

                                socket.close();
                                socketHashMap.remove(clientId);
                                break;
                            }
                            System.out.println("Client " + clientId + " responded:" + line);
                            feedback(clientId, line);
                            line = "";

                        }
                    } catch (IOException ioe){
                        System.out.println("Exception when reading  from client " + clientId);
                        try {
                            socket.close();
                        } catch (IOException ioe2){

                            System.out.println("Cannot close socket when exception occured");
                        }
                        socketHashMap.remove(clientId);
                        System.out.println("Cannot read from client");
                    }



                };
            }.start();


        }


    }






}
